package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "List project";

    public static final String NAME = "project-list";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Sort sort = this.askEntitySort();
        final List<Project> projects = this.getProjectService().findAll(sort);
        this.renderEntities(projects, "Projects:");
    }

}
