package ru.t1.fpavlov.tm.command.entity.task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Clear task repository";

    public static final String NAME = "task-clear";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        this.getTaskService().clear();
    }

}
