package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    List<Project> findAll(Sort sort);

    void remove(final Project project);

    Project findById(final String id);

    Project findByIndex(final Integer index);

    Project update(final Project project, final String name, final String description);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    Project removeById(final String id);

    Project removeByIndex(final Integer index);

    Project changeStatus(final Project project, final Status status);

    Project changeStatusById(final String id, final Status status);

    Project changeStatusByIndex(final Integer index, final Status status);

}
