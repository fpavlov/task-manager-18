package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Change task status by index";

    public static final String NAME = "task-change-status-by-index";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Task entity = this.findByIndex();
        final Status status = this.askEntityStatus();
        this.getTaskService().changeStatus(entity, status);
    }

}
