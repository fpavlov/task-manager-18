package ru.t1.fpavlov.tm.exception.user;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access denied. Login and try again");
    }

}
