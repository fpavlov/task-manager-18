package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.IUserRepository;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fpavlov on 20.12.2021.
 */
public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User create(final String login, final String password) {
        final String passwordHash = HashUtil.salt(password);
        return this.add(new User(login, passwordHash));
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final String passwordHash = HashUtil.salt(password);
        return this.add(new User(login, passwordHash, email));
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final String passwordHash = HashUtil.salt(password);
        return this.add(new User(login, passwordHash, role));
    }

    @Override
    public User add(final User user) {
        this.users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(this.users);
    }

    @Override
    public User findById(final String id) {
        User foundUser = null;
        for (final User user : this.users) {
            if (user.getId().equals(id)) {
                foundUser = user;
                break;
            }
        }
        return foundUser;
    }

    @Override
    public User findByLogin(final String login) {
        User foundUser = null;
        for (final User user : this.users) {
            if (user.getLogin().equals(login)) {
                foundUser = user;
                break;
            }
        }
        return foundUser;
    }

    @Override
    public User findByEmail(final String email) {
        User foundUser = null;
        for (final User user : this.users) {
            if (user.getEmail().equals(email)) ;
            break;
        }
        return foundUser;
    }

    @Override
    public User remove(final User user) {
        this.users.remove(user);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        Boolean isItExist = false;
        for (final User user : this.users) {
            if (user.getLogin().equals(login)) {
                isItExist = true;
                break;
            }
        }
        return isItExist;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        Boolean isItExist = false;
        for (final User user : this.users) {
            if (user.getLogin().equals(email)) {
                isItExist = true;
                break;
            }
        }
        return isItExist;
    }

}
