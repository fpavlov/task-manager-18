package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.util.TerminalUtil;

/**
 * Created by fpavlov on 09.12.2021.
 */
public final class TaskUnbindToProjectCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Unbind task to project";

    public static final String NAME = "unbind-task-project";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        System.out.println("Enter the project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter the task id:");
        final String taskId = TerminalUtil.nextLine();
        this.getProjectTaskService().unbindTaskToProject(projectId, taskId);
    }

}
