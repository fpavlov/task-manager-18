package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.api.service.IProjectTaskService;
import ru.t1.fpavlov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.fpavlov.tm.exception.entity.TaskNotFoundException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/**
 * Created by fpavlov on 26.11.2021.
 */
public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        final Project project = this.projectRepository.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task task = this.taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = this.taskRepository.findAllByProjectId(project.getId());
        for (final Task item : tasks) this.taskRepository.remove(item);
        this.projectRepository.remove(project);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final Project project = this.projectRepository.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final List<Task> tasks = this.taskRepository.findAllByProjectId(projectId);
        for (final Task item : tasks) this.taskRepository.remove(item);
        this.projectRepository.remove(project);
    }

    @Override
    public void unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        final Project project = this.projectRepository.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task task = this.taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
