package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.model.Task;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Update Task by index";

    public static final String NAME = "task-update-by-index";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Task entity = this.findByIndex();
        final String name = this.askEntityName();
        final String description = this.askEntityDescription();
        this.getTaskService().update(entity, name, description);
    }

}
