package ru.t1.fpavlov.tm.command.user;

import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Registry a new user in the system";

    public static final String NAME = "registry";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final String login = this.input("Login:");
        final String password = this.input("Password:");
        final String email = this.input("E-mail:");
        final User user = this.getAuthService().registry(login, password, email);
        this.renderUser(user);
    }

}
