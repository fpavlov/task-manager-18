package ru.t1.fpavlov.tm.api.service;

import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

import java.util.List;

/**
 * Created by fpavlov on 20.12.2021.
 */
public interface IUserService {

    User create(final String login, final String password);

    User create(final String login, final String password, final String email);

    User create(final String login, final String password, final Role role);

    User add(User user);

    List<User> findAll();

    User findById(final String id);

    User findByLogin(final String login);

    User findByEmail(final String email);

    User remove(final User user);

    User removeById(final String id);

    User removeByLogin(final String login);

    User removeByEmail(final String email);

    User setPassword(final String id, final String password);

    User updateUser(final String id,
                    final String firstName,
                    final String lastName,
                    final String middleName);

    Boolean isLoginExist(final String login);

    Boolean isEmailExist(final String email);

}
