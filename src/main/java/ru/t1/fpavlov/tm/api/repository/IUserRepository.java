package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

import java.util.List;

/**
 * Created by fpavlov on 19.12.2021.
 */
public interface IUserRepository {

    User create(final String login, final String password);

    User create(final String login, final String password, final String email);

    User create(final String login, final String password, final Role role);

    User add(final User user);

    List<User> findAll();

    User findById(final String id);

    User findByLogin(final String login);

    User findByEmail(final String email);

    User remove(final User user);

    Boolean isLoginExist(final String login);

    Boolean isEmailExist(final String email);

}
