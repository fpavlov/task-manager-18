package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;

/*
 * Created by fpavlov on 04.10.2021.
 */
public interface ICommandRepository {

    void add(final AbstractCommand command);

    AbstractCommand getCommandByArgument(final String commandArgument);

    AbstractCommand getCommandByName(final String commandName);

    Collection<AbstractCommand> getTerminalCommands();

}
